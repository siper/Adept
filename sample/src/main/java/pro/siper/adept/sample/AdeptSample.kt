package pro.siper.adept.sample

import android.app.Application
import com.facebook.drawee.backends.pipeline.Fresco

class AdeptSample : Application() {
    override fun onCreate() {
        super.onCreate()
        Fresco.initialize(this)
    }
}
