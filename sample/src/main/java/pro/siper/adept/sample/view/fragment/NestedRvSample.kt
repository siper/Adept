package pro.siper.adept.sample.view.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager.HORIZONTAL
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.list_fragment.*
import pro.siper.adept.core.Adept
import pro.siper.adept.sample.R
import pro.siper.adept.sample.decorator.HorizontalSpaceItemDecorator
import pro.siper.adept.sample.decorator.VerticalSpaceItemDecorator
import pro.siper.adept.sample.entity.ui.NestedList

class NestedRvSample : Fragment() {
    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.list_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val adapter = Adept()
                .addRenderer<NestedList>(R.layout.nested_rv_item) { data, viewBinder, payloads ->
                    viewBinder.apply {
                        val adapter = Adept()
                                .addRenderer<String>(R.layout.small_card_item) { data, viewBinder, payloads ->
                                    viewBinder.text(R.id.title, data)
                                }
                        adapter.updateDataset(data.list)
                        adapter(R.id.nestedContent, adapter)

                        val lm = LinearLayoutManager(requireContext())
                        lm.orientation = HORIZONTAL
                        layoutManager(R.id.nestedContent, lm)

                        with<RecyclerView>(R.id.nestedContent) {
                            if (it.itemDecorationCount == 0)
                                it.addItemDecoration(VerticalSpaceItemDecorator(24))
                        }
                    }
                }
                .saveNestedRvStates()
                .attachTo(content)
        val lm = LinearLayoutManager(requireContext())
        content.layoutManager = lm

        content.addItemDecoration(HorizontalSpaceItemDecorator(24))

        val list = mutableListOf<NestedList>()
        for (i in 1..100) {
            val nestedList = mutableListOf<String>()
            for (j in 1..500) {
                nestedList.add(j.toString())
            }
            list.add(NestedList(nestedList))
        }
        adapter.updateDataset(list)
        content.visibility = View.VISIBLE
        progress.visibility = View.GONE
    }
}