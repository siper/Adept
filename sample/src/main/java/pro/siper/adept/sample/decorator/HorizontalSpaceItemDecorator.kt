package pro.siper.adept.sample.decorator

import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.RecyclerView

class HorizontalSpaceItemDecorator(
        private val horizontalSpaceHeight: Int,
        private val marginFirst: Boolean = true
) : RecyclerView.ItemDecoration() {

    override fun getItemOffsets(outRect: Rect,
                                view: View,
                                parent: RecyclerView,
                                state: RecyclerView.State) {
        parent.adapter?.let {
            if (parent.getChildAdapterPosition(view) != it.itemCount - 1 && marginFirst) {
                outRect.top = horizontalSpaceHeight
            } else {
                outRect.bottom = horizontalSpaceHeight
            }
        }
    }
}