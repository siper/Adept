package pro.siper.adept.sample.view.fragment

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.list_fragment.*
import pro.siper.adept.core.Adept
import pro.siper.adept.extension.glide.glide
import pro.siper.adept.extension.picasso.picasso
import pro.siper.adept.sample.R
import pro.siper.adept.sample.api.UnsplashApi
import pro.siper.adept.sample.entity.api.UnsplashItem
import pro.siper.adept.sample.entity.ui.BigImage
import pro.siper.adept.sample.entity.ui.SmallImageLeft
import pro.siper.adept.sample.entity.ui.SmallImageRight
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class ImageLoadersSampleFragment : Fragment() {
    private var call: Call<List<UnsplashItem>> = getNewCall(1)
    private val picasso
        get() = Picasso.get()

    private val adapter = Adept()
    private val callback = object : Callback<List<UnsplashItem>> {
        override fun onFailure(call: Call<List<UnsplashItem>>?, t: Throwable?) {}

        @Suppress("IMPLICIT_CAST_TO_ANY")
        override fun onResponse(call: Call<List<UnsplashItem>>?,
                                response: Response<List<UnsplashItem>>?) {
            if (response?.body() != null && response.isSuccessful) {
                val res = response.body()!!
                        .mapIndexed { index, unsplashItem ->
                            return@mapIndexed when (index % 3) {
                                0 -> BigImage(unsplashItem)
                                1 -> SmallImageRight(unsplashItem)
                                else -> SmallImageLeft(unsplashItem)
                            }
                        }
                adapter.updateDataset(adapter.dataset + res)
                // If you use DiffUtil notifyDataSetChanged() not required
                // adapter.notifyDataSetChanged()
                showContent()
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.list_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        showProgress()
        content.layoutManager = LinearLayoutManager(requireContext())

        adapter
                .addRenderer<SmallImageLeft>(R.layout.small_image_left_item) { data, viewBinder, payloads ->
                    val item = data.unsplashItem
                    viewBinder.apply {
                        text(R.id.created_at, "${item?.createdAt}")
                        text(R.id.width_height, "${item?.width}x${item?.height}")

                        picasso(R.id.image, item?.urls?.regular, picasso) {
                            it.fit()
                        }

                        click(R.id.card) {
                            toast(item?.user?.name)
                        }
                    }
                }
                .addRenderer<SmallImageRight>(R.layout.small_image_right_item) { data, viewBinder, payloads ->
                    val item = data.unsplashItem
                    viewBinder.apply {
                        text(R.id.created_at, "${item?.createdAt}")
                        text(R.id.width_height, "${item?.width}x${item?.height}")

                        picasso(R.id.image, item?.urls?.regular, picasso) {
                            it.fit()
                        }

                        click(R.id.card) {
                            toast(item?.user?.name)
                        }
                    }
                }
                .addRenderer<BigImage>(R.layout.big_image_item) { data, viewBinder, payloads ->
                    val item = data.unsplashItem
                    viewBinder.apply {
                        text(R.id.author, "${item?.user?.firstName} ${item?.user?.lastName}")

                        glide(R.id.image, item?.urls?.regular)
                        glide(R.id.author_avatar, item?.user?.profileImage?.medium)

                        click(R.id.card) {
                            toast(item?.user?.name)
                        }
                    }

                }
                .useEndlessScrolling(R.layout.loading_view) { currentPage, totalItemsCount ->
                    call = getNewCall(currentPage + 1)
                    call.enqueue(callback)
                    return@useEndlessScrolling true
                }
                .useDiffUtil()
                .attachTo(content)
        call.enqueue(callback)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        if (!call.isCanceled) {
            call.cancel()
        }
    }

    private fun showContent() {
        content.visibility = View.VISIBLE
        progress.visibility = View.GONE
    }

    private fun showProgress() {
        content.visibility = View.GONE
        progress.visibility = View.VISIBLE
    }

    private fun getNewCall(page: Int): Call<List<UnsplashItem>> {
        return getUnsplashApi(getRetrofit(Gson()))
                .listPhotos(
                        "295c8ef8cf56b4af9a4d3d960758f0daa2941011a5b123519be9c760d38ac131",
                        page
                )
    }

    private fun getRetrofit(gson: Gson): Retrofit {
        return Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(gson))
                .baseUrl("https://api.unsplash.com/")
                .build()
    }

    private fun getUnsplashApi(retrofit: Retrofit): UnsplashApi {
        return retrofit.create(UnsplashApi::class.java)
    }

    private fun toast(message: String?, context: Context = requireContext()) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }
}