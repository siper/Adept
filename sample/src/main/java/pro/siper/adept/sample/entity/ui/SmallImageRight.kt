package pro.siper.adept.sample.entity.ui

import pro.siper.adept.sample.entity.api.UnsplashItem

data class SmallImageRight(val unsplashItem: UnsplashItem?)
