package pro.siper.adept.sample.api

import pro.siper.adept.sample.entity.api.UnsplashItem
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query


interface UnsplashApi {
    @GET("photos/?")
    fun listPhotos(@Query("client_id") clientId: String,
                   @Query("page") page: Int = 1,
                   @Query("per_page") perPage: Int = 100): Call<List<UnsplashItem>>
}