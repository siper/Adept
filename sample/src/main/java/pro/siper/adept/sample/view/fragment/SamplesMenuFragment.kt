package pro.siper.adept.sample.view.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.samples_menu_fragment.*
import pro.siper.adept.sample.R

class SamplesMenuFragment : Fragment() {
    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.samples_menu_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        imageLoaders.setOnClickListener {
            attachFragment(ImageLoadersSampleFragment())
        }
        nestedList.setOnClickListener {
            attachFragment(NestedRvSample())
        }
        fresco.setOnClickListener {
            attachFragment(FrescoSampleFragment())
        }
        experimental.setOnClickListener {
            attachFragment(ExperimentalSampleFragment())
        }
    }

    private fun attachFragment(fr: Fragment) {
        requireFragmentManager()
                .beginTransaction()
                .replace(R.id.container, fr)
                .addToBackStack(null)
                .commit()
    }
}