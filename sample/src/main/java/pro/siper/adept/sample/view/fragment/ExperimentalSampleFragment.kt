package pro.siper.adept.sample.view.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.list_fragment.*
import kotlinx.android.synthetic.main.nested_rv_item.*
import pro.siper.adept.core.Adept
import pro.siper.adept.experimental.ExperimentalViewBinder
import pro.siper.adept.experimental.addExperimentalRenderer
import pro.siper.adept.sample.R
import pro.siper.adept.sample.decorator.HorizontalSpaceItemDecorator
import pro.siper.adept.sample.decorator.VerticalSpaceItemDecorator
import pro.siper.adept.sample.entity.ui.NestedList

class ExperimentalSampleFragment : Fragment() {
    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.list_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val adapter = Adept()
                .addExperimentalRenderer<NestedList>(R.layout.nested_rv_item) { data, viewBinder, payloads ->
                    viewBinder.apply {
                        val adapter = Adept()
                                .addRenderer<String>(R.layout.small_card_item) { data, viewBinder, payloads ->
                                    viewBinder.text(R.id.title, data)
                                }
                        adapter.updateDataset(data.list)
                        nestedContent.adapter = adapter

                        val lm = LinearLayoutManager(requireContext())
                        lm.orientation = LinearLayoutManager.HORIZONTAL
                        nestedContent.layoutManager = lm

                        if (nestedContent.itemDecorationCount == 0) {
                            nestedContent.addItemDecoration(VerticalSpaceItemDecorator(24))
                        }
                    }
                }
// saveNestedRvStates() doesn't work with experimental renderer,
// if you want to save nested RecyclerView states use default renderer or save it states manually
// .saveNestedRvStates()
                .attachTo(content)
        val lm = LinearLayoutManager(requireContext())
        content.layoutManager = lm

        content.addItemDecoration(HorizontalSpaceItemDecorator(24))

        val list = mutableListOf<NestedList>()
        for (i in 1..100) {
            val nestedList = mutableListOf<String>()
            for (j in 1..500) {
                nestedList.add(j.toString())
            }
            list.add(NestedList(nestedList))
        }
        adapter.updateDataset(list)
        content.visibility = View.VISIBLE
        progress.visibility = View.GONE
    }
}