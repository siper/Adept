# Adept

Simple multiview adapter for RecyclerView without ViewHolder written in Kotlin

# Features

* Multiview support
* AndroidX compatible
* No ViewHolder
* Auto DiffUtils
* Fluent API
* Endless scrolling support
* Nested RecyclerView state saving support
* Payloads support
* Easy Picasso, Glide or Fresco usage with extensions

# Installation

### 1. Add the JCenter repository to your build file
```gradle
allprojects {
	repositories {
		...
		jcenter()
	}
}
```
### 2. Add the dependency
```gradle
ext {
    adept_version = '2.0.1'
}

dependencies {
    // Core library module
    implementation "pro.siper.adept:adept-core:$adept_version"
    // Picasso extension
    implementation "pro.siper.adept:adept-extension-picasso:$adept_version"
    // Glide extension
    implementation "pro.siper.adept:adept-extension-glide:$adept_version"
    // Fresco extension
    implementation "pro.siper.adept:adept-extension-fresco:$adept_version"
    // Kotlin experimental extension
    implementation "pro.siper.adept:adept-extension-experimental:$adept_version"
}
```

# Usage

### 1. Create your Adapter
```kotlin
val adapter = Adept()
```
### 2. Register view renderer
```kotlin
adapter.addRenderer<SmallImageLeft>(R.layout.small_image_left_item) { data, viewBinder, payloads ->
                           val item = data.unsplashItem
                           viewBinder.apply {
                               text(R.id.created_at, "${item?.createdAt}")
                               text(R.id.width_height, "${item?.width}x${item?.height}")

                               picasso(R.id.image, item?.urls?.regular, picasso) {
                                   it.fit()
                               }

                               click(R.id.card) {
                                   toast(item?.user?.name)
                               }
                           }
                       }
```
### 3. Attach you adapter to RecyclerView
```kotlin
recyclerView.adapter = adapter

// or

adapter.attachTo(recyclerView)
```
### 4. Update dataset
```kotlin
adapter.updateDataset(list)
```
# Kotlin experimental extension
If you want to use ExperimentalViewBinder you should add kotlin-android-extensions plugin into you gradle.build file
```gradle
apply plugin: 'kotlin-android-extensions'
```
and turn on experimental functions:
```gradle
androidExtensions {
    experimental = true
}
```
# Third-party libraries used in sample project

* [Picasso](http://square.github.io/picasso/)
* [Glide](https://github.com/bumptech/glide)
* [Fresco](http://frescolib.org/)
* [Retrofit2](http://square.github.io/retrofit/)
* [Gson](https://github.com/google/gson)

# License

```
Copyright 2018 Kirill Zhukov

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```