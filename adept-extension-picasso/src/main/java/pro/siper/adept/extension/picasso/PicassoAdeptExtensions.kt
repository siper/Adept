package pro.siper.adept.extension.picasso

import android.net.Uri
import android.widget.ImageView
import com.squareup.picasso.Picasso
import com.squareup.picasso.RequestCreator
import pro.siper.adept.core.DefaultViewBinder
import java.io.File

fun DefaultViewBinder.picasso(id: Int,
                              url: String?,
                              fit: Boolean = false,
                              picasso: Picasso = Picasso.get()): DefaultViewBinder {
    val request = picasso.load(url)
    if (fit) {
        request.fit()
    }
    request.into(viewHolder.findView<ImageView>(id))
    return this
}

fun DefaultViewBinder.picasso(id: Int,
                              uri: Uri?,
                              fit: Boolean = false,
                              picasso: Picasso = Picasso.get()): DefaultViewBinder {
    val request = picasso.load(uri)
    if (fit) {
        request.fit()
    }
    request.into(viewHolder.findView<ImageView>(id))
    return this
}

fun DefaultViewBinder.picasso(id: Int,
                              file: File,
                              fit: Boolean = false,
                              picasso: Picasso = Picasso.get()): DefaultViewBinder {
    val request = picasso.load(file)
    if (fit) {
        request.fit()
    }
    request.into(viewHolder.findView<ImageView>(id))
    return this
}

fun DefaultViewBinder.picasso(id: Int,
                              resId: Int,
                              fit: Boolean = false,
                              picasso: Picasso = Picasso.get()): DefaultViewBinder {
    val request = picasso.load(resId)
    if (fit) {
        request.fit()
    }
    request.into(viewHolder.findView<ImageView>(id))
    return this
}

fun DefaultViewBinder.picasso(id: Int,
                              url: String?,
                              picasso: Picasso = Picasso.get(),
                              action: (request: RequestCreator) -> RequestCreator): DefaultViewBinder {
    action.invoke(picasso.load(url)).into(viewHolder.findView<ImageView>(id))
    return this
}

fun DefaultViewBinder.picasso(id: Int,
                              uri: Uri?,
                              picasso: Picasso = Picasso.get(),
                              action: (request: RequestCreator) -> RequestCreator): DefaultViewBinder {
    action.invoke(picasso.load(uri)).into(viewHolder.findView<ImageView>(id))
    return this
}

fun DefaultViewBinder.picasso(id: Int,
                              file: File,
                              picasso: Picasso = Picasso.get(),
                              action: (request: RequestCreator) -> RequestCreator): DefaultViewBinder {
    action.invoke(picasso.load(file)).into(viewHolder.findView<ImageView>(id))
    return this
}

fun DefaultViewBinder.picasso(id: Int,
                              resId: Int,
                              picasso: Picasso = Picasso.get(),
                              action: (request: RequestCreator) -> RequestCreator): DefaultViewBinder {
    action.invoke(picasso.load(resId)).into(viewHolder.findView<ImageView>(id))
    return this
}