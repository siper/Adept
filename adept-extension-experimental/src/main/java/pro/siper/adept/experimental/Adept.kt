package pro.siper.adept.experimental

import pro.siper.adept.core.*

inline fun <reified T> Adept.addExperimentalRenderer(
        layoutId: Int,
        renderer: ItemViewRenderer<T, ExperimentalViewBinder>): Adept {
    addCustomRenderer(layoutId, renderer)
    if (customBinder == null) {
        customBinder = object : BinderSetter<ExperimentalViewBinder> {
            override fun getCustomViewBinder(holder: AdeptViewHolder): ExperimentalViewBinder {
                return ExperimentalViewBinder(holder)
            }
        }
    }
    return this
}

inline fun <reified T> Adept.addExperimentalRenderer(
        layoutId: Int,
        crossinline renderer: (data: T, viewBinder: ExperimentalViewBinder, payloads: MutableList<Any>) -> Unit): Adept {
    addExperimentalRenderer(layoutId, object : ItemViewRenderer<T, ExperimentalViewBinder>() {
        override fun render(data: T, viewBinder: ExperimentalViewBinder, payloads: MutableList<Any>) {
            renderer.invoke(data, viewBinder, payloads)
        }
    })
    return this
}