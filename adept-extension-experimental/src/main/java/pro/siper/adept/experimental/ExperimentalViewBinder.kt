package pro.siper.adept.experimental

import android.view.View
import kotlinx.android.extensions.CacheImplementation
import kotlinx.android.extensions.ContainerOptions
import kotlinx.android.extensions.LayoutContainer
import pro.siper.adept.core.AdeptViewHolder
import pro.siper.adept.core.ViewBinder

@ContainerOptions(cache = CacheImplementation.SPARSE_ARRAY)
class ExperimentalViewBinder(holder: AdeptViewHolder) : ViewBinder(holder), LayoutContainer {
    override val containerView: View? = holder.itemView
}