package pro.siper.adept.extension.glide

import android.net.Uri
import com.bumptech.glide.Glide
import com.bumptech.glide.RequestBuilder
import com.bumptech.glide.RequestManager
import pro.siper.adept.core.DefaultViewBinder
import java.io.File


fun DefaultViewBinder.glide(id: Int,
                            url: String?,
                            glide: RequestManager = Glide.with(viewHolder.itemView.context)): DefaultViewBinder {
    glide.load(url).into(viewHolder.findView(id))
    return this
}

fun DefaultViewBinder.glide(id: Int,
                            uri: Uri?,
                            glide: RequestManager = Glide.with(viewHolder.itemView.context)): DefaultViewBinder {
    glide.load(uri).into(viewHolder.findView(id))
    return this
}

fun DefaultViewBinder.glide(id: Int,
                            file: File,
                            glide: RequestManager = Glide.with(viewHolder.itemView.context)): DefaultViewBinder {
    glide.load(file).into(viewHolder.findView(id))
    return this
}

fun DefaultViewBinder.glide(id: Int,
                            resId: Int,
                            glide: RequestManager = Glide.with(viewHolder.itemView.context)): DefaultViewBinder {
    glide.load(resId).into(viewHolder.findView(id))
    return this
}


fun DefaultViewBinder.glide(id: Int,
                            url: String?,
                            glide: RequestManager = Glide.with(viewHolder.itemView.context),
                            action: (request: RequestBuilder<*>) -> RequestBuilder<*>): DefaultViewBinder {
    action.invoke(glide.load(url)).into(viewHolder.findView(id))
    return this
}

fun DefaultViewBinder.glide(id: Int,
                            uri: Uri?,
                            glide: RequestManager = Glide.with(viewHolder.itemView.context),
                            action: (request: RequestBuilder<*>) -> RequestBuilder<*>): DefaultViewBinder {
    action.invoke(glide.load(uri)).into(viewHolder.findView(id))
    return this
}

fun DefaultViewBinder.glide(id: Int,
                            file: File,
                            glide: RequestManager = Glide.with(viewHolder.itemView.context),
                            action: (request: RequestBuilder<*>) -> RequestBuilder<*>): DefaultViewBinder {
    action.invoke(glide.load(file)).into(viewHolder.findView(id))
    return this
}

fun DefaultViewBinder.glide(id: Int,
                            resId: Int,
                            glide: RequestManager = Glide.with(viewHolder.itemView.context),
                            action: (request: RequestBuilder<*>) -> RequestBuilder<*>): DefaultViewBinder {
    action.invoke(glide.load(resId)).into(viewHolder.findView(id))
    return this
}