package pro.siper.adept.core

interface BinderSetter<VB> {
    fun getCustomViewBinder(holder: AdeptViewHolder): VB
}