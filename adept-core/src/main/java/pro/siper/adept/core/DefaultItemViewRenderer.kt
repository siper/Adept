package pro.siper.adept.core

abstract class DefaultItemViewRenderer<D> : ItemViewRenderer<D, DefaultViewBinder>()