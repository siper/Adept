package pro.siper.adept.core

import android.graphics.Bitmap
import android.graphics.Typeface
import android.graphics.drawable.Drawable
import android.util.TypedValue
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class DefaultViewBinder(viewHolder: AdeptViewHolder) : ViewBinder(viewHolder) {

    fun text(id: Int, text: String): DefaultViewBinder {
        val tv = viewHolder.findView<TextView>(id)
        tv.text = text
        return this
    }

    fun image(id: Int, drawable: Drawable): DefaultViewBinder {
        val iv = viewHolder.findView<ImageView>(id)
        iv.setImageDrawable(drawable)
        return this
    }

    fun image(id: Int, resId: Int): DefaultViewBinder {
        val iv = viewHolder.findView<ImageView>(id)
        iv.setImageResource(resId)
        return this
    }

    fun image(id: Int, bitmap: Bitmap): DefaultViewBinder {
        val iv = viewHolder.findView<ImageView>(id)
        iv.setImageBitmap(bitmap)
        return this
    }

    fun background(id: Int, resId: Int): DefaultViewBinder {
        viewHolder.findView<View>(id).setBackgroundResource(resId)
        return this
    }

    fun click(id: Int, listener: View.OnClickListener): DefaultViewBinder {
        viewHolder.findView<View>(id).setOnClickListener(listener)
        return this
    }

    fun click(id: Int, action: () -> Unit): DefaultViewBinder {
        viewHolder.findView<View>(id).setOnClickListener {
            action.invoke()
        }
        return this
    }

    fun longClick(id: Int, listener: View.OnLongClickListener): DefaultViewBinder {
        viewHolder.findView<View>(id).setOnLongClickListener(listener)
        return this
    }

    fun longClick(id: Int, action: () -> Boolean): DefaultViewBinder {
        viewHolder.findView<View>(id).setOnLongClickListener {
            action.invoke()
        }
        return this
    }

    fun visible(id: Int): DefaultViewBinder {
        viewHolder.findView<View>(id).visibility = View.VISIBLE
        return this
    }

    fun invisible(id: Int): DefaultViewBinder {
        viewHolder.findView<View>(id).visibility = View.INVISIBLE
        return this
    }

    fun gone(id: Int): DefaultViewBinder {
        viewHolder.findView<View>(id).visibility = View.GONE
        return this
    }

    fun enable(id: Int): DefaultViewBinder {
        viewHolder.findView<View>(id).isEnabled = true
        return this
    }

    fun disable(id: Int): DefaultViewBinder {
        viewHolder.findView<View>(id).isEnabled = false
        return this
    }

    fun select(id: Int): DefaultViewBinder {
        viewHolder.findView<View>(id).isSelected = true
        return this
    }

    fun unselect(id: Int): DefaultViewBinder {
        viewHolder.findView<View>(id).isSelected = false
        return this
    }

    fun textColor(id: Int, color: Int): DefaultViewBinder {
        viewHolder.findView<TextView>(id).setTextColor(color)
        return this
    }

    fun textSize(id: Int, sizeSp: Int): DefaultViewBinder {
        viewHolder.findView<TextView>(id).setTextSize(TypedValue.COMPLEX_UNIT_SP, sizeSp.toFloat())
        return this
    }

    fun typeface(id: Int, typeface: Typeface): DefaultViewBinder {
        viewHolder.findView<TextView>(id).typeface = typeface
        return this
    }

    fun typeface(id: Int, typeface: Typeface, style: Int): DefaultViewBinder {
        viewHolder.findView<TextView>(id).setTypeface(typeface, style)
        return this
    }

    fun alpha(id: Int, alpha: Float): DefaultViewBinder {
        viewHolder.findView<View>(id).alpha = alpha
        return this
    }

    fun <V: View> with(id: Int, action: (v: V) -> Unit): DefaultViewBinder {
        action.invoke(viewHolder.findView(id))
        return this
    }

    fun adapter(id: Int, adapter: RecyclerView.Adapter<*>): DefaultViewBinder {
        viewHolder.findView<RecyclerView>(id).adapter = adapter
        return this
    }

    fun layoutManager(id: Int, layoutManager: RecyclerView.LayoutManager): DefaultViewBinder {
        viewHolder.findView<RecyclerView>(id).layoutManager = layoutManager
        return this
    }

    fun fixedSize(id: Int, hasFixedSize: Boolean): DefaultViewBinder {
        viewHolder.findView<RecyclerView>(id).setHasFixedSize(hasFixedSize)
        return this
    }
}