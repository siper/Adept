package pro.siper.adept.core.diff

import android.os.Bundle

interface DiffUtilCallback {
    fun areItemsTheSame(oldItem: Any?, newItem: Any?): Boolean
    fun areContentsTheSame(oldItem: Any?, newItem: Any?): Boolean
    fun getChangePayload(oldItem: Any?, newItem: Any?): Bundle? = null
}