package pro.siper.adept.core

import android.util.SparseArray

fun <T> SparseArray<T>.forEachWithKey(action: (item: T, key: Int) -> Unit) {
    for (i in 0 until this.size()) {
        action.invoke(valueAt(i), keyAt(i))
    }
}

fun <T> SparseArray<T>.filter(predicate: (T) -> Boolean): SparseArray<T> {
    this.forEachWithKey { item, key ->
        if (!predicate.invoke(item)) {
            remove(key)
        }
    }
    return this
}

fun <T, R> SparseArray<T>.map(transformer: (T) -> R): SparseArray<R> {
    return SparseArray<R>().also {
        this.forEachWithKey { item, key ->
            it.put(key, transformer.invoke(item))
        }
    }
}