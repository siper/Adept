package pro.siper.adept.core

abstract class ViewBinder(val viewHolder: AdeptViewHolder)