package pro.siper.adept.core

abstract class ItemViewRenderer<D, VB> : IItemViewRenderer<D, VB> {
    @Suppress("UNCHECKED_CAST")
    internal fun anyTypeRender(data: Any?, viewBinder: ViewBinder, payloads: MutableList<Any>) {
        render(data as D, viewBinder as VB, payloads)
    }
}