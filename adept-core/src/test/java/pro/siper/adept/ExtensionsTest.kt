package pro.siper.adept

import android.util.SparseArray
import org.junit.Before
import org.junit.Test
import pro.siper.adept.core.filter
import pro.siper.adept.core.forEachWithKey
import pro.siper.adept.core.map

class ExtensionsTest {
    private val array: SparseArray<String> = SparseArray()

    @Before
    fun init() {
        array.apply {
            put(0, "A")
            put(1, "B")
            put(2, "C")
            put(3, "D")
        }
    }

    @Test
    fun `SparseArray for each with key`() {
        array.forEachWithKey { item, key ->
            when(key) {
                0 -> assert(item == "A")
                1 -> assert(item == "B")
                2 -> assert(item == "C")
                3 -> assert(item == "D")
            }
        }
    }

    @Test
    fun `SparseArray filter`() {
        val ar = array.filter { it == "A" || it == "C" }
        assert(ar.indexOfValue("A") == 0)
        assert(ar.indexOfValue("C") == 0)
    }

    @Test
    fun `SparseArray map`() {
        val ar = array.map { "C" }
        ar.forEachWithKey { item, key ->
            assert(item == "C")
        }
    }
}