package pro.siper.adept.entity

data class User(
        val name: String,
        val email: String
)