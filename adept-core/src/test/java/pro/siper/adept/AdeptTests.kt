package pro.siper.adept

import org.junit.Before
import org.junit.Test
import pro.siper.adept.core.Adept
import pro.siper.adept.core.diff.DiffUtilCallback
import pro.siper.adept.entity.User

class AdeptTests {
    lateinit var adapter: Adept

    @Before
    fun init() {
        adapter = Adept()
    }

    @Test
    fun `Item added`() {
        val user = User("John Smith", "john_smith@mail.com")
        adapter.updateDataset(listOf(user))
        assert(adapter.dataset.isNotEmpty())
        assert(adapter.dataset[0] == user)
    }

    @Test
    fun `Renderer added`() {
        val layoutId = 11
        adapter.addRenderer<User>(11) { data, viewBinder, payloads -> }
        assert(adapter.renderers.isNotEmpty())
        assert(adapter.renderers.contains(User::class))
        assert(adapter.layouts[User::class] == layoutId)
    }

    @Test
    fun `Use DiffUtil`() {
        val diffUtilCallback = object : DiffUtilCallback {
            override fun areItemsTheSame(oldItem: Any?, newItem: Any?) = false
            override fun areContentsTheSame(oldItem: Any?, newItem: Any?) = false
        }
        adapter.useDiffUtil(diffUtilCallback)
        assert(adapter.useDiffUtil)
    }

    @Test
    fun `Use endless scrolling`() {
        adapter.useEndlessScrolling { currentPage, totalItemsCount ->  true }
        assert(adapter.useEndlessScrolling)
    }
}