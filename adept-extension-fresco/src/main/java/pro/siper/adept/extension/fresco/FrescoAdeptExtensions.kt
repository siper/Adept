package pro.siper.adept.extension.fresco

import android.net.Uri
import com.facebook.drawee.generic.GenericDraweeHierarchy
import com.facebook.drawee.view.SimpleDraweeView
import pro.siper.adept.core.DefaultViewBinder
import java.io.File

fun DefaultViewBinder.fresco(id: Int,
                             url: String?): DefaultViewBinder {
    viewHolder.findView<SimpleDraweeView>(id).setImageURI(url)
    return this
}

fun DefaultViewBinder.fresco(id: Int,
                             uri: Uri?): DefaultViewBinder {
    viewHolder.findView<SimpleDraweeView>(id).setImageURI(uri, null)
    return this
}

fun DefaultViewBinder.fresco(id: Int,
                             file: File): DefaultViewBinder {
    viewHolder.findView<SimpleDraweeView>(id).setImageURI(Uri.fromFile(file), null)
    return this
}

fun DefaultViewBinder.fresco(id: Int,
                             resId: Int): DefaultViewBinder {
    viewHolder
            .findView<SimpleDraweeView>(id)
            .setImageURI(Uri.parse("res:/$resId"), null)
    return this
}

fun DefaultViewBinder.fresco(
        id: Int,
        url: String?,
        hierarchy: GenericDraweeHierarchy): DefaultViewBinder {
    val drawee = viewHolder.findView<SimpleDraweeView>(id)
    drawee.setImageURI(url)
    drawee.hierarchy = hierarchy
    return this
}

fun DefaultViewBinder.fresco(
        id: Int,
        uri: Uri?,
        hierarchy: GenericDraweeHierarchy): DefaultViewBinder {
    val drawee = viewHolder.findView<SimpleDraweeView>(id)
    drawee.setImageURI(uri, null)
    drawee.hierarchy = hierarchy
    return this
}

fun DefaultViewBinder.fresco(
        id: Int,
        file: File,
        hierarchy: GenericDraweeHierarchy): DefaultViewBinder {
    val drawee = viewHolder.findView<SimpleDraweeView>(id)
    drawee.setImageURI(Uri.fromFile(file), null)
    drawee.hierarchy = hierarchy
    return this
}

fun DefaultViewBinder.fresco(
        id: Int,
        resId: Int,
        hierarchy: GenericDraweeHierarchy): DefaultViewBinder {
    val drawee = viewHolder.findView<SimpleDraweeView>(id)
    drawee.setActualImageResource(resId)
    drawee.hierarchy = hierarchy
    return this
}

fun DefaultViewBinder.fresco(
        id: Int,
        url: String?,
        hierarchy: () -> GenericDraweeHierarchy): DefaultViewBinder {
    fresco(id, url, hierarchy.invoke())
    return this
}

fun DefaultViewBinder.fresco(
        id: Int,
        uri: Uri?,
        hierarchy: () -> GenericDraweeHierarchy): DefaultViewBinder {
    fresco(id, uri, hierarchy.invoke())
    return this
}

fun DefaultViewBinder.fresco(
        id: Int,
        file: File,
        hierarchy: () -> GenericDraweeHierarchy): DefaultViewBinder {
    fresco(id, file, hierarchy.invoke())
    return this
}

fun DefaultViewBinder.fresco(
        id: Int,
        resId: Int,
        hierarchy: () -> GenericDraweeHierarchy): DefaultViewBinder {
    fresco(id, resId, hierarchy.invoke())
    return this
}